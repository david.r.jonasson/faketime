#!/bin/bash
set -e
openssl genrsa -out rootCA.key 4096
faketime '2034-09-06 08:01:07' openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 365 -out rootCA.crt
