# Faketime

Do you want to create certificates in the future or the distant past?

Then this is for you.

It makes use of [faketime](https://github.com/wolfcw/libfaketime) and [OpenSSL](https://github.com/openssl/openssl) in a docker container. No need to install anything.

## Usage
First adjust the `data/create_cert.sh` file to your needs. Currently it creates a Root CA.

### Build the image

```
docker build -t faketime .
```

### Run the container:

```
docker run -it --rm --mount type=bind,source="$(pwd)"/data,target=/data faketime bash ./create_cert.sh
```

Then follow the command line instructions from OpenSSL.

### Check result
Look at the dates of the certificate:

```
openssl x509 -in data/rootCA.crt -text -noout
```
